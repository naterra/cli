<?php

/**
 *
 * Command Class
 * 
 */
abstract class Command {
    /**
     * @var CommandEntry[]
     */
    static protected $methods = [];

    function __construct()
    {
        /* register methods from a class inherited by this class */
        $reflectionClass = new ReflectionClass($this);
        foreach ($reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC) as $reflectionMethod) {

            /* skip static methods and magic methods */
            if($reflectionMethod->isStatic() || strpos($reflectionMethod->getName(),'__')===0)
                continue;

            self::$methods[$reflectionMethod->getName()] = new CommandEntry($reflectionMethod->getName(), $this, $reflectionClass, $reflectionMethod);
        }

    }

    /**
     * @param $method
     * @return bool|CommandEntry
     */
    public static function getMethod($method)
    {
        return isset(self::$methods[$method]) ? self::$methods[$method] : false;
    }


    public static function run($scenario) {
        /* explode by space */		 
        $args = (array)explode(" ", $scenario);
		
        /* first property from array will be command name  */
        $commandName = strtolower(array_shift($args));

        /* other properties map by rules: the arguments are double quoted "" with multiple arguments being comma separated. Empty arguments will be deleted*/
        $opts = array_filter(array_map("trim", (array)str_getcsv(implode(" ",$args), ",", '"')));

        if(!($command = self::getMethod($commandName)))
            throw new Exception("unknown command: $commandName\nUse: help - see the details of the call");

        if($command->reflectionMethod->getNumberOfRequiredParameters()>count($opts)) {
            self::help($commandName);
            throw new Exception("$commandName - this command requires at least " . $command->reflectionMethod->getNumberOfRequiredParameters() . " required parameter");
        }
 
        $result = call_user_func_array(array($command->class, $commandName), $opts);

        return $result;
    }

    /**
     * The help method displays help for themselves or other methods
     * @param null $command
     * @throws Exception
     */
    public function help($command = null) {
        $commandEntry = null;

        if($command && !($commandEntry = self::getMethod($command)))
            throw new Exception("help not found for command: $command");

        /* if command defined */
        if($commandEntry) {

            echo "\n".str_repeat('-',20)."\n";
            echo "$command ";

            /* print command arguments */
            foreach ($commandEntry->reflectionMethod->getParameters() as $p) {
                /* required or not */
                echo !$p->isDefaultValueAvailable() ? " <{$p->name}>" : " [{$p->name}]";
            }

            echo "\n".str_repeat('-',20)."\n";

            //Разбираем PHPdoc
            $doc= preg_replace('#^(/\*\*| *\*/?) ?#ms','', $commandEntry->reflectionMethod->getDocComment());
            $doc = implode("\n", array_filter(explode("\n", $doc)));
            echo $doc."\n\n";

        } else { /* if the command isn't defined */
            if(self::$methods) {
                echo "There are following commands with functions:\n";
                echo str_repeat('-', 20)."\n";
            }

            /* loop the methods */
            foreach(self::$methods as $methodName=>$methodEntry) {
                $doc = preg_replace("#^@.*#ms", "",
                    preg_replace('#^(/\*\*| *\*/?) ?#ms', '',
                        $methodEntry->reflectionMethod->getDocComment()
                    )
                );
                $doc = implode("\n", array_filter(explode("\n", $doc)));

                /* print class name and its description */
                echo "$methodName";

                /* print arguments */
                foreach ($methodEntry->reflectionMethod->getParameters() as $p) {
                    /* required or not */
                    echo !$p->isDefaultValueAvailable() ? " <{$p->name}>" : " [{$p->name}]";
                }

                echo " - $doc\n\n";
            }

            if(self::$methods) {
                echo str_repeat('-', 20)."\n";
                echo "For more details use: help <command>\n\n";
            }
        }
    }

}


class CommandEntry {
    public
        $commandName,
        $class,
        $reflectionClass,
        $reflectionMethod;

    function __construct($commandName, $class, $reflectionClass=null, $reflectionMethod=null)
    {
        $this->commandName = $commandName;

        $this->class = $class;

        $this->reflectionClass = $reflectionClass ?:new ReflectionClass($class);

        $this->reflectionMethod = $reflectionMethod ?: new ReflectionMethod($class, $commandName);
    }


}