<?php

/**
 * Class Model
 *
 * This class allowing to work with model
 * set up and receiving fields of model 
 *
 */
abstract class Model {

    /**
     * check the field for existing
     * @param $field
     * @return bool
     */
    function __isset($field)
    {
        $_field = "_$field";
        return property_exists($this, $_field);
    }


    public function __call($name, $arguments)
    {
        $prefix = substr($name,0,3);

        switch ($prefix) {
            case "get":
                return $this->{strtolower(substr($name,3))};
                break;
            case "set":
                return $this->{strtolower(substr($name,3))} = $arguments[0];
                break;
        }

        throw new Exception("Call to undefined method $name");
    }


    /**
     * Magic Method run when writing data to inaccessible properties:
     * @param $field
     * @param $value
     * @return bool
     * @throws Exception
     */
    public function __set($field, $value = null)
    {
        $_field = "_$field";

        if(!property_exists($this, $_field))
            throw new Exception("incorrect field '$field'!");

        $this->validate($field, $value);

        /* set default value is empty */
        if (!$value && $this::$fields[$field]['default']) {
            $value = $this::$fields[$field]['default'];
        }

        /* in addition, could be used before and after the function ...
        if(method_exists($this, "beforeSet$field"))
            $this->{"beforeSet$field"}($value);
        */

        $this->$_field = $value;

        /*
        if(method_exists($this, "afterSet$field"))
            $this->{"beforeSet$field"}($value);
        */

        return true;
    }

    /**
     * Getting the value of models field  :
     *      $model->getName() return protected $_name from the model;
     *
     * @param $field
     * @return mixed
     * @throws Exception
     */
    public function __get($field) {
        $_field = "_$field";

        if(!property_exists($this, $_field))
            throw new Exception("incorrect field '$field'!");

        return $this->$_field;
    }


    /**
     * this method working with $model::$fields
     * checking the fields for required status 
     *
     * @param $field
     * @param $value
     * @throws Exception
     */
    protected function validate($field, $value){
        if(!$value && isset($this::$fields[$field]['required']) && $this::$fields[$field]['required']) {
            throw new Exception("'$field' is required field!");

        } elseif ($value && isset($this::$fields[$field]['variants']) && !in_array($value, (array)$this::$fields[$field]['variants'], true)) {
            throw new Exception("the wrong choice for the field '$field'!");
        }

    }
}