<?php
include_once "contributors.model.php";
include_once "command.class.php";
include_once "storage.class.php";

/**
 * class Contributors
 *
 * Manage contributors records  
 *  
 */
class ContributorsController extends Command {

    /** @var Contributor[]  */
    protected $contributors = array();

    /** @var Storage  */
    protected $storage = array();

    function __construct()
    {
        parent::__construct();
        $this->storage=Storage::instance();
        $this->contributors = $this->storage->getData();
    }

    /**
     * add a new contributor, status optional ("assigned" or "unassigned", defaults to "unassigned").
     * @param $name
     * @param $location
     * @param null $status [assigned|unassigned]
     * @return bool
     * @throws Exception
     */
    public function add_contributor($name, $location, $status=null) {

        if(isset($this->contributors[$name]))
            throw new Exception("the contributor with the same name [$name] exists");

        $contributor = new Contributor($name, $location, $status);
        $this->contributors[$contributor->getName()] = $contributor;
        $this->storage->saveData($this->contributors);
        printf (
            "-- contributor %s added!\n",
            $contributor->getName()
        );
        return true;
    }

    /**
     * Create random contributors records for testing purpose
     * @param int $count
     * @return bool
     */
    public function example($count=10) {
        for($i=0; $i<$count; $i++) {
            $name = "contributor_$i";
            $location= rand()%5>2 ? "New York" : "Los Angeles";
            $status = rand()%5>2 ? Contributor::STATUS_ASSIGNED : Contributor::STATUS_NOT_ASSIGNED;
            $this->add_contributor($name, $location, $status);
        }
        return true;
    }


    /**
     * remove a contributor.
     * @param $name
     * @return bool
     * @throws Exception
     */
    public function del_contributor($name) {
        if(!isset($this->contributors[$name]))
            throw new Exception("not found contributor with name: $name");

        unset($this->contributors[$name]);
        $this->storage->saveData($this->contributors);

        printf (
            "-- contributor with name [%s] was deleted!\n", $name
        );

        return true;
    }


    /**
     * mark a contributor as being assigned
     * @param $name
     * @return bool
     * @throws Exception
     */
    public function assign_contributor($name) {
        if(!isset($this->contributors[$name]))
            throw new Exception("not found contributor with name: $name");

        $this->contributors[$name]->setStatus(contributor::STATUS_ASSIGNED);
        $this->storage->saveData($this->contributors);

        printf (
            "-- mark a contributor with name: %s [%s] as being assigned!\n", $name, $this->contributors[$name]->getName()
        );

        return true;
    }

    /**
     * mark a contributor as being unassigned.
     * @param $name
     * @return bool
     * @throws Exception
     */
    public function unassign_contributor($name) {
        if(!isset($this->contributors[$name]))
            throw new Exception("not found contributor with name: $name");

        $this->contributors[$name]->setStatus(contributor::STATUS_NOT_ASSIGNED);
        $this->storage->saveData($this->contributors);

        printf (
            "-- mark a contributor with name: %s [%s] as being unassigned!\n", $name, $this->contributors[$name]->getName()
        );

        return true;
    }

    /**
     * show all extant contributors sorted by optional criteria
     * @param $sort [sort_alpha|sort_location|sort_status]
     */
    public function show_all($sort=null){
        $this->show($sort);
    }

    /**
     * show all extant contributors in a location
     * @param $location
     */
    public function show_by_location($location){
        $this->show('filter_location', $location);
    }

    /**
     * show all extant contributors with the designated status
     * @param $status [assigned|unassigned]
     */
    public function show_by_status($status){
        $this->show('filter_status', $status);
    }

    /**
     * show all extant contributors sorted by optional criteria,
     *   use params: list [sort_location|sort_status|sort_alpha] [filter_location "value" &| filter_name "value" &| filter_status "value"]
     *   Example:
     *     contributors list sort_location filter_location "Los Angeles" filter_status assigned
     * @return bool
     */
    public function show($criteria1=null, $criteria2=null, $criteria3=null, $etc=null/*func_get_args()*/) {
        $contributor = (array)$this->contributors;
        $sortField = null;
        $sort = null;
        $filters=null;

        if($args = func_get_args()) {
            for($i=0; $i<count($args); $i++) {
                /* if sorting option exist, could be used just one */
                if(strpos($args[$i], 'sort_')===0 && !isset($sort)) {
                    $sort = $args[$i];
                    continue;
                }

                /* if filter option exist, could be used many */
                if(strpos($args[$i], 'filter_')===0) {
                    /* if filter value is empty, skip it */
                    if($args[$i+1]) {
                        $filters[substr($args[$i], 7)] = $args[$i + 1];
                    }
                }
            }
        }

        /* use sorting ... */
        if($sort == "sort_location") {
            $sortField = 'location';
        } else if($sort == "sort_status") {
            $sortField = 'status';
        } else if($sort == "sort_alpha"){
            $sortField = 'name';
        }

        if($sortField) {
            uasort($contributor, function (Contributor $a, Contributor $b) use ($sortField) {
                if ($a->$sortField == $b->$sortField) {
                    return 0;
                }
                return ($a->$sortField < $b->$sortField) ? -1 : 1;
            });
        }

        /* use filters */
        if($filters) {
            $contributor = array_filter($contributor, function(Contributor $o) use ($filters) {
                $validate = true;
                foreach($filters as $field=>$value) {
                    /* if field not exists, the search is not valid */
                    if(!isset($o->$field)) {
                        $validate = false;
                        break;

                    /* if field value not equal filter value - element is not valid */
                    } else if (strtolower($o->$field) != strtolower($value)) {
                        $validate = false;
                        break;
                    }
                }
			/* if $validate is true, array_filter return array */
                return $validate;
            });
        }


        if($contributor) {
            #$mask = "|%12.12s| %-20.30s | %-15.15s |%-10.10s |\n";
            #printf($mask, 'Id', 'Name', 'Location', 'Status');

            $mask = "-- %s (%s, %s)\n";
            foreach ($contributor as $contributor) {
                printf($mask, $contributor->getName(), $contributor->getLocation(), $contributor->getStatus());
            }
        }

        if($filters && !$contributor ) {
            echo "no record could be found ...\n";
        } else if (!$contributor){
            echo "no contributor entries ...\n";
        }
        return true;
    }

}
