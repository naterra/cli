#!/usr/bin/php
<?php
if (PHP_SAPI !== 'cli')
    die('only use in cli mode');

include_once "command.class.php";

/* include contributor commands class */
include_once 'contributors.controller.php';
new ContributorsController();

echo <<<EOF

WELCOME !!!------------------------------------------------------------------
It uses interactive mode!
Use: "help" - for more help.
Arguments are double quoted "" with multiple arguments being comma separated.
Exits the program (via ctrl-C or 'quit')
-----------------------------------------------------------------------------

EOF;

$name = substr(basename(__FILE__),0,-4);
$fp = fopen("php://stdin", "r");

while ( true ) {
    echo "$name>";
    $in = trim(fgets($fp));
    if (!$in) continue;
    if(strtolower(trim($in)) == "quit") break;

    try {
        Command::run($in);
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage() . "\n\n";
    }
}
