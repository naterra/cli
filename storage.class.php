<?php

/* activate session if not existing */
if(!session_id())
    session_start();

/**
 * Class Storage
 *  
 */
class Storage {
    protected static $instance;

    public static function instance() {
        if (self::$instance === null)
            self::$instance = new self;
        return self::$instance;
    }

    protected function __construct(){}

    /**
     * Getting the data from storage
     */
    public function getData() {
        if(isset($_SESSION["store_".get_called_class()]))
            return $_SESSION["store_".__CLASS__];

        return null;
    }

    /**
     * Save data
     */
    public function saveData($values) {
        $_SESSION["store_".get_called_class()] = $values;

        return true;
    }
}