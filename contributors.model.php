<?php
include_once "model.class.php";

/**
 * Class Contributor
 *
 * @method getId()
 *
 * @method setName()
 * @method getName()
 *
 * @method setLocation()
 * @method getLocation()
 *
 * @method setStatus()
 * @method getStatus()
 *
 */
class Contributor extends Model {
    const STATUS_ASSIGNED = "assigned";
    const STATUS_NOT_ASSIGNED = "unassigned";

    /** @var  int  Id Contributor */
    protected $_id;

    /** @var  string Name Contributor */
    protected $_name;

    /** @var  string Location */
    protected $_location;

    /** @var  string Status */
    protected $_status;


    public static $fields = [
        'name' => [
            'required' => true,
        ],

        'location' => [
            'required' => true,
        ],

        'status' => [
            'default' => self::STATUS_NOT_ASSIGNED,
            'variants' => [
                self::STATUS_NOT_ASSIGNED,
                self::STATUS_ASSIGNED
            ],
        ]
    ];

    public function __construct($name, $location, $status = null){
        $this->setId();
        $this->setName($name);
        $this->setLocation($location);
        $this->setStatus($status);
    }

    protected function setId()
    {
        $this->id = substr(md5(session_id() . microtime()),0,12);
    }

}